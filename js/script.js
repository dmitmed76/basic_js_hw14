const button = document.querySelector('.btn');
const html = document.documentElement;

button.addEventListener('click', chengTheme);

function chengTheme() {
	if (localStorage.getItem('theme') === 'dark') {
		localStorage.removeItem('theme')
	} else {
		localStorage.setItem('theme', 'dark');
	};
	addDarkTheme()
};

function addDarkTheme() {
	if (localStorage.getItem('theme') === 'dark') {
		html.classList.add('dark');
	} else {
		html.classList.remove('dark');
	}
}
addDarkTheme()